<?php
list($_,$local,$file)=$argv;
$local=($local==="true");
$replacements=[
    "%PKG%"=>"assets",
    "%CLASSNAME%"=>ucfirst(explode("/",explode(".",$file)[0])[count(explode("/",explode(".",$file)[0]))-1]),
    "%FILENAME%"=>$file
];
$conts=str_replace(array_keys($replacements),array_values($replacements),file_get_contents("templates/".explode(".",$file)[1].($local?"_local":"").".as"));
file_put_contents("src/assets/".ucfirst(explode("/",explode(".",$file)[0])[count(explode("/",explode(".",$file)[0]))-1]).".as",$conts);
