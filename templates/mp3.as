package %PKG% {
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	public class %CLASSNAME%
	{
		public var sound:Sound;
		public var soundChannel:SoundChannel;
		public function %CLASSNAME%():void
		{
			sound=new Sound(new URLRequest(Settings.ASSETS_URL+"&asset=%CLASSNAME%&assettype=mp3"));
		}
		public function play():void
		{
			soundChannel=sound.play();
		}
		public function stop():void
		{
			soundChannel.stop();
		}
	}
}
