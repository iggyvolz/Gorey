package {
  import assets.*;
  public class Settings{
    public static const COMMIT_HASH:String="%COMMIT_HASH%";
    public static const COMMIT_MSG:String="%COMMIT_MSG%";
    public static const GIT_DESCRIBE:String="%GIT_DESCRIBE%";
    public static const LOCAL:Boolean=%LOCAL%;
    public static const ASSETS_URL:String="%ASSETS_URL%";
  }
}
