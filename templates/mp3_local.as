package %PKG% {
	import flash.media.Sound;
	import flash.media.SoundChannel;
	public class %CLASSNAME%
	{
		[Embed(source='/../%FILENAME%')]
		private static const gnome:Class;
		public var sound:Sound;
		public var soundChannel:SoundChannel;
		public function %CLASSNAME%():void
		{
			sound=new gnome();
		}
		public function play():void
		{
			soundChannel=sound.play();
		}
		public function stop():void
		{
			soundChannel.stop();
		}
	}
}
