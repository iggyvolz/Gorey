package %PKG% {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.display.Loader;
	public class %CLASSNAME% extends Sprite {
		private var gfx:Bitmap;
		private var ldr:Loader=new Loader();
		public function %CLASSNAME%():void
		{
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			ldr.load(new URLRequest(Settings.ASSETS_URL+"&asset=%CLASSNAME%&assettype=png"));
		}
		private function onLoadComplete(e:Event):void
		{
			gfx=ldr.content as Bitmap;
			addChild(gfx);
		}
	}
}
