package
{
  import assets.*;
  import flash.display.Sprite;
  import flash.text.TextField;
  import flash.text.TextFormat;
  import flash.events.MouseEvent;
  import flash.events.Event;
  public class Instructions extends Sprite
  {
    public static var instance:Instructions;
    private var backButton:TextField = new TextField();
    private var textbox:TextField = new TextField();
    private var textFormat:TextFormat=new TextFormat('Arial',72,0x000000);
    private var textboxFormat:TextFormat=new TextFormat('Arial',12,0x000000);
    private var titleImage:Title=new Title();
    public function Instructions():void
    {
      instance=this;

      backButton.defaultTextFormat=textFormat;
      backButton.x=50;
      backButton.y=500;
      backButton.width=180;
      backButton.height=80;
      backButton.textColor = 0x000000;
      backButton.background = false;
      backButton.borderColor = 0x000000;
      backButton.border = true;
      backButton.text = "Back";
      backButton.type="dynamic";
      backButton.selectable=false;
      backButton.addEventListener(MouseEvent.CLICK, onBackClick);

      textbox.defaultTextFormat=textboxFormat;
      textbox.text="You're Al Gore (the guy with the black outline) and the voters pop up and random - they could come in with bias (more red = more likely to vote Bush, more blue = more likely to vote Gore) - you use the arrow keys to move.  If you go up to someone and talk to them (just try to go through them) then they'll be more likely to vote you.\nAdding more advertisements increases the number of people who show, as well as adding side effects based on whether your ads are mean or nice - nice ads will make people who were originally going to vote you feel more strong about their vote and have no effect on people who weren't going to vote you in the first place.  Mean ads make everyone question their vote and draw everyone closer to neutral.  More ads increase the power of meanness or niceness.  You must win 270 electoral votes to win the election - you start off with actual results, minus the top 38 states.";
      textbox.x=0;
      textbox.y=0;
      textbox.width=800;
      textbox.height=650;
      textbox.wordWrap=true;
      textbox.textColor = 0x000000;
      textbox.background = false;
      textbox.border = false;
      textbox.type="dynamic";
      textbox.selectable=false;
      addChild(textbox);
      addChild(backButton);
    }
    private function onBackClick(e:MouseEvent):void
    {
      Main.instance.currentDisplay=Main.instance.menu;
    }
  }
}
