package
{
  import assets.*;
  import flash.display.Sprite;
  import flash.text.TextField;
  import flash.text.TextFormat;
  import flash.events.MouseEvent;
  import flash.events.Event;
  public class Credits extends Sprite
  {
    public static var instance:Credits;
    private var backButton:TextField = new TextField();
    private var textbox:TextField = new TextField();
    private var textFormat:TextFormat=new TextFormat('Arial',72,0x000000);
    private var textboxFormat:TextFormat=new TextFormat('Arial',12,0x000000);
    private var titleImage:Title=new Title();
    public function Credits():void
    {
      instance=this;

      backButton.defaultTextFormat=textFormat;
      backButton.x=50;
      backButton.y=500;
      backButton.width=180;
      backButton.height=80;
      backButton.textColor = 0x000000;
      backButton.background = false;
      backButton.borderColor = 0x000000;
      backButton.border = true;
      backButton.text = "Back";
      backButton.type="dynamic";
      backButton.selectable=false;
      backButton.addEventListener(MouseEvent.CLICK, onBackClick);

      textbox.defaultTextFormat=textboxFormat;
      textbox.text="Elections information from Wikipedia article \"United States presidential election, 2000\".\nFor this game, we are completely ignoring that Maine and Nebraska give partial electoral votes - it was too complex to program :D\nTitle image by Peter Richards, used with permission, all rights reserved.";
      textbox.x=0;
      textbox.y=0;
      textbox.width=800;
      textbox.height=650;
      textbox.wordWrap=true;
      textbox.textColor = 0x000000;
      textbox.background = false;
      textbox.border = false;
      textbox.type="dynamic";
      textbox.selectable=false;
      addChild(textbox);
      addChild(backButton);
    }
    private function onBackClick(e:MouseEvent):void
    {
      Main.instance.currentDisplay=Main.instance.menu;
    }
  }
}
