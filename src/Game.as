package
{
  import flash.events.Event;
  import flash.display.Sprite;
  import flash.utils.getTimer;
  public class Game extends Sprite
  {
    public static var instance:Game;
    public var voters:Array=[];
    public var alGore:AlGore=new AlGore(200,200);
    private var nextSpawnTime:uint;
    private var endTime:uint;
    private var f:uint=0;
    public function Game():void
    {
      instance=this;
      addChild(alGore);
      addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
    }
    private function onAddedToStage(e:Event):void
    {
      endTime=getTimer()+20000;
      addEventListener(Event.ENTER_FRAME,onEnterFrame);
    }
    private function onEnterFrame(e:Event):void
    {
      if(getTimer()>endTime)
      {
        Main.instance.currentDisplay=Main.instance.interlude;
        removeEventListener(Event.ENTER_FRAME,onEnterFrame);
      }
      if(getTimer()>nextSpawnTime)
      {
        var i:uint=voters.length;
        while(true)
        {
          var _x:uint=Math.random()*780;
          var _y:uint=(Math.random()*390)+140;
          if(AlGore.instance.isHitting(_x,_y))
          {
            break;
          }
        }
        voters[i]=new Voter(2500,_x,_y);
        addChild(voters[i]);
        nextSpawnTime=getTimer()+2000;
      }
    }
  }
}
