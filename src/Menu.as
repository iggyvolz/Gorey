package
{
  import assets.*;
  import flash.display.Sprite;
  import flash.text.TextField;
  import flash.text.TextFormat;
  import flash.events.MouseEvent;
  import flash.events.Event;
  public class Menu extends Sprite
  {
    public static var instance:Menu;
    private var playButton:TextField = new TextField();
    private var creditsButton:TextField = new TextField();
    private var instructionsButton:TextField = new TextField();
    private var textFormat:TextFormat=new TextFormat('Arial',72,0x000000);
    private var titleImage:Title=new Title();
    public function Menu():void
    {
      instance=this;

      playButton.defaultTextFormat=textFormat;
      playButton.x=50;
      playButton.y=400;
      playButton.width=150;
      playButton.height=80;
      playButton.textColor = 0x000000;
      playButton.background = false;
      playButton.borderColor = 0x000000;
      playButton.border = true;
      playButton.text = "Play";
      playButton.type="dynamic";
      playButton.selectable=false;
      playButton.addEventListener(MouseEvent.CLICK, onPlayClick);
      addChild(playButton);

      creditsButton.defaultTextFormat=textFormat;
      creditsButton.x=500;
      creditsButton.y=400;
      creditsButton.width=230;
      creditsButton.height=80;
      creditsButton.textColor = 0x000000;
      creditsButton.background = false;
      creditsButton.borderColor = 0x000000;
      creditsButton.border = true;
      creditsButton.text = "Credits";
      creditsButton.type="dynamic";
      creditsButton.selectable=false;
      creditsButton.addEventListener(MouseEvent.CLICK, onCreditsClick);
      addChild(creditsButton);

      instructionsButton.defaultTextFormat=textFormat;
      instructionsButton.x=175;
      instructionsButton.y=500;
      instructionsButton.width=375;
      instructionsButton.height=80;
      instructionsButton.textColor = 0x000000;
      instructionsButton.background = false;
      instructionsButton.borderColor = 0x000000;
      instructionsButton.border = true;
      instructionsButton.text = "Instructions";
      instructionsButton.type="dynamic";
      instructionsButton.selectable=false;
      instructionsButton.addEventListener(MouseEvent.CLICK, onInstructionsClick);
      addChild(instructionsButton);

      titleImage.x=200;
      titleImage.y=0;
      addChild(titleImage);
    }
    private function onPlayClick(e:MouseEvent):void
    {
      Main.instance.currentDisplay=Main.instance.interlude;
    }
    private function onCreditsClick(e:MouseEvent):void
    {
      Main.instance.currentDisplay=Main.instance.credits;
    }
    private function onInstructionsClick(e:MouseEvent):void
    {
      Main.instance.currentDisplay=Main.instance.instructions;
    }
  }
}
