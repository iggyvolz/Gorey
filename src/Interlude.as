package
{
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.text.TextField;
  import flash.text.TextFormat;
  import flash.events.MouseEvent;
  public class Interlude extends Sprite
  {
    public var bushVotes:uint;
    public var goreVotes:uint;
    public var bushElectoral:uint=195;
    public var goreElectoral:uint=204;
    private var textboxFormat:TextFormat=new TextFormat('Arial',12,0x000000);
    public var states:Array=["Florida","New Mexico","Wisconson","Iowa","Oregon","New Hampshire","Minnesota","Missouri","Ohio","Nevada","Tennessee","Pennsylvania"];
    public var stateVotes:Array=[25,5,11,7,7,4,10,11,21,4,11,23];
    public var results:Array=[]; // true - Gore wins
    private var message:TextField = new TextField();
    private var first:Boolean=true;
    private var nextButton:TextField = new TextField();
    private var nextButtonFormat:TextFormat=new TextFormat('Arial',72,0x000000);
    public function Interlude():void
    {
      addEventListener(Event.ADDED_TO_STAGE,handleAddedToStage);
      message.defaultTextFormat=textboxFormat;
      message.x=0;
      message.y=0;
      message.width=800;
      message.height=650;
      message.wordWrap=true;
      message.textColor = 0x000000;
      message.background = false;
      message.border = false;
      message.type="dynamic";
      message.selectable=false;
      addChild(message);
      nextButton.defaultTextFormat=nextButtonFormat;
      nextButton.x=50;
      nextButton.y=400;
      nextButton.width=150;
      nextButton.height=80;
      nextButton.textColor = 0x000000;
      nextButton.background = false;
      nextButton.borderColor = 0x000000;
      nextButton.border = true;
      nextButton.text = "Next";
      nextButton.type="dynamic";
      nextButton.selectable=false;
      nextButton.addEventListener(MouseEvent.CLICK, onNextClick);
      addChild(nextButton);
    }
    private function handleAddedToStage(e:Event):void
    {
      message.text="";
      if(first)
      {
        first=false;
      }
      else
      {
        var result:Boolean=(bushVotes>goreVotes); // Give benefit of the doubt with tie.
        var state:String=states[results.length];
        var votes:uint=stateVotes[results.length];
        results[results.length]=result;
        if(result)
        {
          message.appendText("Bush won "+state+" and its "+votes+" votes by a count of "+bushVotes+" to "+goreVotes+"\n");
          bushElectoral+=votes;
        }
        else
        {
          message.appendText("Gore won "+state+" and its "+votes+" votes by a count of "+goreVotes+" to "+bushVotes+"\n");
          goreElectoral+=votes;
        }
      }
      if(goreElectoral>269)
      {
        Main.instance.finale.status="W";
        Main.instance.currentDisplay=Main.instance.finale;
      }
      else if(bushElectoral>269)
      {
        Main.instance.finale.status="L";
        Main.instance.currentDisplay=Main.instance.finale;
      }
      else if(results.length==states.length)
      {
        Main.instance.finale.status="T";
        Main.instance.currentDisplay=Main.instance.finale;
      }
      if(bushElectoral<goreElectoral)
      {
        message.appendText("Current electoral votes: Gore leads "+goreElectoral+" electors to "+bushElectoral+" electors\n");
      }
      else if(bushElectoral>goreElectoral)
      {
        message.appendText("Current electoral votes: Bush leads "+bushElectoral+" electors to "+goreElectoral+" electors\n");
      }
      else
      {
        message.appendText("Current electoral votes: Tied at "+goreElectoral+" electors\n");
      }
      message.appendText("Next up: "+states[results.length]+" with "+stateVotes[results.length]+" electoral votes");
    }
    private function onNextClick(e:Event):void
    {
      bushVotes=0;
      goreVotes=0;
      Main.instance.currentDisplay=Main.instance.game;
    }
  }
}
