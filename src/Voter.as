package
{
  import assets.*;
  import flash.events.Event;
  import flash.geom.ColorTransform;
  import flash.utils.getTimer;
  public class Voter extends Person
  {
    private var tendency:uint; //0 - 100% chance of vote against, 5000 - 100% chance of vote for.  Crafted this way to mimic original behavior - it takes 5 seconds (5000ms) to go from one end to the other, so tendency+=<milliseconds> each frame will do this behavior.
    private var elapsedTime:int;
    private var lastTime:int;
    private var _x:uint;
    private var _y:uint;
    public var active:Boolean=true;
    public function Voter(_t:uint,__x:uint,__y:uint):void
    {
      tendency=_t-1; // Use the convince function to add in the extra point.
      convince(0);
      _x=__x;
      _y=__y;
      addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
      addEventListener(Event.REMOVED_FROM_STAGE,onRemovedFromStage);
    }
    private function onAddedToStage(e:Event):void
    {
      x=_x;
      y=_y;
      lastTime=getTimer();
      addEventListener(Event.ENTER_FRAME,onEnterFrame);
    }
    private function onRemovedFromStage(e:Event):void
    {
      removeEventListener(Event.ENTER_FRAME,onEnterFrame);
    }
    private function onEnterFrame(e:Event):void
    {
      elapsedTime+=getTimer()-lastTime;
      if(elapsedTime>5000)
      {
        if(Math.random()*5000<tendency)
        {
          Main.instance.interlude.goreVotes++;
        }
        else
        {
          Main.instance.interlude.bushVotes++;
        }
        Game.instance.removeChild(this);
        active=false;
      }
      lastTime=getTimer();
    }
    public function convince(n:uint):void
    {
      if(tendency+n>5000)
      {
        tendency=5000;
        return;
      }
      tendency+=n;
      var colorInfo:ColorTransform = this.transform.colorTransform;
      colorInfo.redOffset=(5000-tendency)*256/5000;
      colorInfo.blueOffset=tendency*256/5000;
      this.transform.colorTransform=colorInfo;
    }
    public function isHitting(__x:uint,__y:uint):Boolean
    {
      switch(true)
      {
        case x>__x+18:
        case x+18<__x:
        case y>__y+35:
        case y+35<__y:
          return false;
      }
      return true;
    }
  }
}
