package
{
  import assets.*;
  import flash.events.Event;
  import flash.events.KeyboardEvent;
  import flash.utils.getTimer;
  public class AlGore extends Person
  {
    private var _x:uint;
    private var _y:uint;
    private var lastTime:int;
    public static const SPEED:uint=5;
    private var keys:Array=[false,false,false,false]; // left, up, right, down
    public static var instance:AlGore;
    public function AlGore(__x:uint,__y:uint):void
    {
      instance=this;
      _x=__x;
      _y=__y;
      addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
    }
    private function onAddedToStage(e:Event):void
    {
      x=_x;
      y=_y;
      stage.addEventListener(KeyboardEvent.KEY_DOWN,onKeyDown);
      stage.addEventListener(KeyboardEvent.KEY_UP,onKeyUp);
      lastTime=getTimer();
      addEventListener(Event.ENTER_FRAME,onEnterFrame);
    }
    private function onEnterFrame(e:Event):void
    {
      var elapsedTime:int=getTimer()-lastTime;
      lastTime=getTimer();
      _x=x;
      _y=y;
      if(keys[0])
      {
        x-=SPEED;
      }
      else if(keys[2])
      {
        x+=SPEED;
      }
      if(keys[1])
      {
        y-=SPEED;
      }
      else if(keys[3])
      {
        y+=SPEED;
      }
      for(var i:uint=0;i<Game.instance.voters.length;i++)
      {
        if(Game.instance.voters[i].active&&Game.instance.voters[i].isHitting(x,y))
        {
          x=_x;
          y=_y;
          Game.instance.voters[i].convince(elapsedTime);
          return;
        }
      }
    }
    private function onKeyDown(e:KeyboardEvent):void
    {
      if(e.keyCode>=37&&e.keyCode<=40)
      {
        keys[e.keyCode-37]=true;
      }
    }
    private function onKeyUp(e:KeyboardEvent):void
    {
      if(e.keyCode>=37&&e.keyCode<=40)
      {
        keys[e.keyCode-37]=false;
      }
    }
    public function isHitting(__x:uint,__y:uint):Boolean
    {
      switch(true)
      {
        case x>__x+18:
        case x+18<__x:
        case y>__y+35:
        case y+35<__y:
          return true;
      }
      return false;
    }
  }
}
