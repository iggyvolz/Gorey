package
{
  import assets.*;
  import flash.display.Sprite;
  public class Main extends Sprite
  {
    public static var instance:Main;
    private var _currentDisplay:Sprite;
    public var menu:Menu=new Menu();
    public var game:Game=new Game();
    public var credits:Credits=new Credits();
    public var instructions:Instructions=new Instructions();
    public var interlude:Interlude=new Interlude();
    public var finale:Finale=new Finale();
    public function Main():void
    {
      instance=this;
      currentDisplay=menu;
    }
    public function get currentDisplay():Sprite
    {
      return _currentDisplay;
    }
    public function set currentDisplay(_cd:Sprite):void
    {
      if(currentDisplay)
      {
        removeChild(currentDisplay);
      }
      _currentDisplay=_cd;
      addChild(currentDisplay);
    }
  }
}
